<?php include('header.php');

	$request  = $_POST;
	if (isset($_POST) && $request) {
		$from = $request['email'];
		$subject = $request['subject'];
		$message = $request['message'];
		$to = "business@appsdelta.com";
         
         
         $message .= "<b>User Email.'".$from."'</b>";
         
         
         $header = "From:business@appsdelta.com \r\n";
         $header .= "MIME-Version: 1.0\r\n";
         $header .= "Content-type: text/html\r\n";
         
         $retval = mail ($to,$subject,$message,$header);
         
         if( $retval == true ) {
            //echo "Message sent successfully...";
         }else {
            //echo "Message could not be sent...";
         }
	}
	
?>


		<!--/ End Header -->
		
		<!-- Breadcrumb -->
		<div class="breadcrumbs overlay" style="background-image:url('https://via.placeholder.com/1600x500')">
			<div class="container">
				<div class="row">
					<div class="col-12">
						<div class="bread-inner">
							<!-- Bread Menu -->
							<div class="bread-menu">
								<ul>
									<li><a href="index.php">Home</a></li>
									<li><a href="contact.php">Contact</a></li>
								</ul>
							</div>
							<!-- Bread Title -->
							<div class="bread-title"><h2>Our Address</h2></div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!--/ End Breadcrumb -->
		
		<!-- Contact Us -->
		<section class="contact-us section-space">
			<div class="container">
				<div class="row">
					<div class="col-lg-7 col-md-7 col-12">
						<!-- Contact Form -->
						<h3 id="aftersendemail"><h3>
						<div class="contact-form-area m-top-30">
							<h4>Get In Touch</h4>
							<form class="form" method="post" id='contactusForm'>
								<div class="row">
									<div class="col-lg-6 col-md-6 col-12">
										<div class="form-group">
											<div class="icon"><i class="fa fa-user"></i></div>
											<input type="text" required name="first_name" id='first_name' placeholder="First Name">
										</div>
									</div>
									<div class="col-lg-6 col-md-6 col-12">
										<div class="form-group">
											<div class="icon"><i class="fa fa-user"></i></div>
											<input type="text" required name="last_name" id ='last_name' placeholder="Last Name">
										</div>
									</div>
									<div class="col-lg-6 col-md-6 col-12">
										<div class="form-group">
											<div class="icon"><i class="fa fa-envelope"></i></div>
											<input type="email" required name="email" id='email' placeholder="Type email id">
										</div>
									</div>
									<div class="col-lg-6 col-md-6 col-12">
										<div class="form-group">
											<div class="icon"><i class="fa fa-tag"></i></div>
											<input type="text" required name="subject" id='subject' placeholder="Type Subjects">
										</div>
									</div>
									<div class="col-12">
										<div class="form-group textarea">
											<div class="icon"><i class="fa fa-pencil"></i></div>
											<textarea required type="textarea" name="message" id='message' rows="5"></textarea>
										</div>
									</div>
									<div class="col-12">
										<div class="form-group button">
											<button type="submit" onclick="submitContact()" class="bizwheel-btn theme-2">Send Now</button>
										</div>
										
									</div>
								</div>
							</div>
						</form>
						<!--/ End contact Form -->
					</div>
					<div class="col-lg-5 col-md-5 col-12">
						<div class="contact-box-main m-top-30">
							<div class="contact-title">
								<h2>Good Talks Make Good Projects</h2>
								<p>While we are good at sign language, there are various other simpler ways to connect with us. Talk to us in person.</p>
							</div>
							<!-- Single Contact -->
							<!-- <div class="single-contact-box">
								<div class="c-icon"><i class="fa fa-clock-o"></i></div>
								<div class="c-text">
									<h4>Opening Hour</h4>
									<p>Friday - Saturday<br>08AM - 10PM (everyday)</p>
								</div>
							</div> -->
							<!--/ End Single Contact -->
							<!-- Single Contact -->
							<div class="single-contact-box">
								<div class="c-icon"><i class="fa fa-phone"></i></div>
								<div class="c-text">
									<h4>Call Us Now</h4>
									<p>Mob.: 858 797 8359<br> Mob.: 965 070 6465</p>
								</div>
							</div>
							<!--/ End Single Contact -->
							<!-- Single Contact -->
							<div class="single-contact-box">
								<div class="c-icon"><i class="fa fa-envelope-o"></i></div>
								<div class="c-text">
									<h4>Email Us</h4>
									<p>business@appsdelta.com<br>info@appsdelta.com</p>
								</div>
							</div>
							<!--/ End Single Contact -->
							<div class="button">
								<a href="#" class="bizwheel-btn theme-1">Our Works<i class="fa fa-angle-right"></i></a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>	
		<!--/ End Contact Us -->
		<script>
function submitContact() {
			var formData = $("#contactusForm").serialize();
			let first_name = $('#first_name').val();
			let last_name = $('#last_name').val();
			let email = $('#email').val();
			let subject = $('#subject').val();
			let message = $('#message').val();
			if(first_name && last_name && email && subject && message){
				$.ajax({
				url: '/contact.php',
				type: "POST",
				data: formData,
				success: function(data, textStatus, jqXHR) {
					$('#aftersendemail').html('Message sent successfully...');
					$("#contactusForm").trigger('reset');
				},
				error: function(jqXHR, textStatus, errorThrown) {
					alert('Error occurred!');
					$('#aftersendemail').html('Message not sent !...');
				}

			});
	}
}
</script>
		<!-- Footer -->
		<?php include('footer.php')?>