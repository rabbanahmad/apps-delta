<?php include('header.php')?>
		<!--/ End Header -->
		
		<!-- Breadcrumb -->
		<div class="breadcrumbs overlay" style="background-image:url('https://via.placeholder.com/1600x500')">
			<div class="container">
				<div class="row">
					<div class="col-12">
						<div class="bread-inner">
							<!-- Bread Menu -->
							<div class="bread-menu">
								<ul>
									<li><a href="index.php">Home</a></li>
									<li><a href="services.php">Services</a></li>
								</ul>
							</div>
							<!-- Bread Title -->
							<div class="bread-title"><h2>Our Services</h2></div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!--/ End Breadcrumb -->
		
		<!-- Services -->
		<section class="services section-bg section-space">
			<div class="container">
				<div class="row">
					<div class="col-12">
						<div class="section-title  style2 text-center">
							<div class="section-top">
								<h1><span>Creative</span><b>Service We Provide</b></h1><h4>We provide quality service &amp; support..</h4>
							</div>
							<div class="section-bottom">
								<div class="text-style-two">
									<p>Our team of software developers can build exceptional applications across a breadth of devices including desktop, mobile and tablet.</p>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-4 col-md-6 col-12">
						<!-- Single Service -->
						<div class="single-service" style="text-align: center">
							<div class="service-head" >
								<img src="img/android.png" style="text-align: center;height:200px;" alt="#">
								<div class="icon-bg"><i class="fa fa-handshake-o"></i></div>
							</div>
							<div class="service-content">
								<h4><a href="javascript:void(0);" onclick="scrollToDiv('androidAppDevelopment');" >Android Development</a></h4>
								<p>Android is touted as the right choice for startups and emerging companies.</p>
								<a class="btn" href="javascript:void(0);" onclick="scrollToDiv('androidAppDevelopment');"><i class="fa fa-arrow-circle-o-right"></i>View Service</a>
							</div>
						</div>
						<!--/ End Single Service -->
					</div>
					<div class="col-lg-4 col-md-6 col-12">
						<!-- Single Service -->
						<div class="single-service">
							<div class="service-head" >
								<img src="img/ios_development.png" style="text-align: center;height:200px;" alt="#">
								<div class="icon-bg"><i class="fa fa-html5"></i></div>
							</div>
							<div class="service-content">
								<h4><a href="javascript:void(0);" onclick="scrollToDiv('iOSAppDevelopment');">iOS Development</a></h4>
								<p>iOS application development is known as the best choice for building secure, scalable digital solutions.</p>
								<a class="btn" href="javascript:void(0);" onclick="scrollToDiv('iOSAppDevelopment');"><i class="fa fa-arrow-circle-o-right"></i>View Service</a>
							</div>
						</div>
						<!--/ End Single Service -->
					</div>
					<div class="col-lg-4 col-md-6 col-12">
						<!-- Single Service -->
						<div class="single-service">
							<div class="service-head" >
								<img src="img/web_development.png" style="text-align: center;height:200px;" alt="#">
								<div class="icon-bg"><i class="fa fa-cube"></i></div>
							</div>
							<div class="service-content">
								<h4><a href="javascript:void(0);" onclick="scrollToDiv('webDevelopment');">Web Development</a></h4>
								<p>All this may sound daunting at first, but you don’t need to know everything at once.</p>
								<a class="btn" href="javascript:void(0);" onclick="scrollToDiv('webDevelopment');"><i class="fa fa-arrow-circle-o-right"></i>View Service</a>
							</div>
						</div>
						<!--/ End Single Service -->
					</div>
					<div class="col-lg-4 col-md-6 col-12">
						<!-- Single Service -->
						<div class="single-service">
							<div class="service-head" style="text-align: center">
								<img src="img/nodejs_development.png" style="text-align: center;height:200px;" alt="#">
								<div class="icon-bg"><i class="fa fa-coffee"></i></div>
							</div>
							<div class="service-content">
								<h4><a href="javascript:void(0);" onclick="scrollToDiv('node');">NodeJs Development</a></h4>
								<p>Node.js is an open-source, cross-platform, back-end JavaScript runtime environment that runs on the V8 engine and executes JavaScript code.</p>
								<a class="btn" href="javascript:void(0);" onclick="scrollToDiv('node');"><i class="fa fa-arrow-circle-o-right"></i>View Service</a>
							</div>
						</div>
						<!--/ End Single Service -->
					</div>
					<div class="col-lg-4 col-md-6 col-12">
						<!-- Single Service -->
						<div class="single-service" style="text-align: center">
							<div class="service-head">
								<img src="img/cross.png" style="text-align: center;height:200px;" alt="#">
								<div class="icon-bg"><i class="fa fa-bullhorn"></i></div>
							</div>
							<div class="service-content">
								<h4><a href="javascript:void(0);" onclick="scrollToDiv('crossPlaformDevelopment');">Cross-Platform Development</a></h4>
								<p>Cross-platform mobile development is the creation of software applications that are compatible with multiple mobile operating systems.</p>
								<a class="btn"href="javascript:void(0);" onclick="scrollToDiv('crossPlaformDevelopment');"><i class="fa fa-arrow-circle-o-right"></i>View Service</a>
							</div>
						</div>
						<!--/ End Single Service -->
					</div>
					<div class="col-lg-4 col-md-6 col-12">
						<!-- Single Service -->
						<div class="single-service">
							<div class="service-head" style="text-align: center">
								<img src="img/uiux.png"  style="text-align: center;height:200px;" alt="#">
								<div class="icon-bg"><i class="fa fa-bullseye"></i></div>
							</div>
							<div class="service-content">
								<h4><a href="javascript:void(0);" onclick="scrollToDiv('design');">UI/UX Development</a></h4>
								<p>User experience (UX) design is centered around the satisfaction the user experiences with your software.</p>
								<a class="btn" href="javascript:void(0);" onclick="scrollToDiv('design');"><i class="fa fa-arrow-circle-o-right"></i>View Service</a>
							</div>
						</div>
						<!--/ End Single Service -->
					</div>
				</div>
				<div class="container">
  
        <div class="stack-detail">
            <div id="androidAppDevelopment" class="sec-stack first-sec">
                <div class="stack-heading">
                    <img src="img/android.png" alt="android">
                    <h3 class="android-dev mb-3">Android App Development</h3>
                </div>
                <p>Appsdelta has specialization in Android App Development. Our experienced professionals develop apps
                    that follow the latest trends and future development standards. We have a team whose extensive
                    experience in different programming languages like Kotlin, Java, and hybrid programming languages
                    like Ionic, Flutter, and ReactJS. That is the reason we serve gaming, healthcare, sports, social
                    media, and entertainment sectors altogether efficiently.</p>
                <div class="row">
                    <div class="col-md-3">
                        <ul class="list-unstyled stack-list">
                            <li>Native Android App</li>
                            <li>Android Tablet App</li>
                            <li>Android Games</li>
                        </ul>
                    </div>
                    <div class="col-md-4">
                        <ul class="list-unstyled stack-list">
                            <li>Android Wearable Apps</li>
                            <li>Android Support &amp; Maintenance</li>
                            <li>Upgrade and Migrate Existing iPhone Apps</li>
                        </ul>
                    </div>
                </div>
                <!-- <a href="android-development" class="btn mob-cod-btn mt-3">Know More</a> -->
            </div>
            <hr>
            <div id="iOSAppDevelopment" class="sec-stack second-stack">
                <div class="stack-heading">
                    <img src="img/ios_development.png"  alt="iOS">
                    <h3 class="android-dev mb-3">iOS App Development</h3>
                </div>
                <p>iOS app development is an area that requires honed core skills and exclusive experience. With that
                    exclusive experience in the industry, Appsdelta has a proven record of satisfying businesses and
                    enterprise clients. We have been successful in providing the best user experience without
                    compromising data privacy. Quality in every aspect is what we always focus on.</p>
                <div class="row">
                    <div class="col-md-4">
                        <ul class="list-unstyled stack-list">
                            <li>Native iOS Development</li>
                            <li>Custom iOS App Design</li>
                            <li>Apple iPad App</li>
                            <li>iPhone Games Development</li>
                        </ul>
                    </div>
                    <div class="col-md-4">
                        <ul class="list-unstyled stack-list">
                            <li>iPhone Social Networking Applications</li>
                            <li>iPhone App QA and Testing</li>
                            <li>iPhone Support and Maintenance</li>
                            <li>iPhone eCommerce Applications</li>
                        </ul>
                    </div>
                </div>
                <!-- <a href="ios-development" class="btn mob-cod-btn mt-3">Know More</a> -->
            </div>
            <hr>
            <div id="webDevelopment" class="sec-stack third-stack">
                <div class="stack-heading">
                    <img src="img/web_development.png"  alt="Web Development">
                    <h3 class="android-dev mb-3">Web Development</h3>
                </div>
                <p>Appsdelta provides reliable HTML5 development services to various industry verticals. By using the
                    latest best tools, we build highly creative and innovative solutions for different platforms and
                    devices. Using the best design methodologies, we build quality products and ensure on-time delivery.
                </p>
                <div class="row">
                    <div class="col-md-4">
                        <ul class="list-unstyled stack-list">
                            <li>Custom HTML5 Design &amp; Development</li>
                            <li>HTML5 Website Development</li>
                            <li>HTML5 Application Development</li>
                            <li>HTML5 Mobile App Development</li>
                        </ul>
                    </div>
                    <div class="col-md-4">
                        <ul class="list-unstyled stack-list">
                            <li>HTML5 Plugin Development</li>
                            <li>Silverlight to HTML5 Migration</li>
                            <li>HTML5 and CSS Development</li>
                            <li>PSD to HTML5 Conversions</li>
                        </ul>
                    </div>
                </div>
                <!-- <a href="web-development" class="btn mob-cod-btn mt-3">Know More</a> -->
            </div>
            <hr>
            <div id="crossPlaformDevelopment" class="sec-stack fourth-stack">
                <div class="stack-heading">
                    <img src="img/cross.png" alt="Cross Platform Development">
                    <h3 class="android-dev mb-3">Cross-Platform Development</h3>
                </div>
                <p>Cross-platform app development provides the versatility to app for smooth run on different platforms.
                    Since there are a number of platforms available in the market, so it is important to develop an app
                    that runs equally efficient on every platform. At Appsdelta, we strongly focus on this factor so that
                    the changing trend doesn’t diminish the value of your app.</p>
                <div class="row">
                    <div class="col-md-3">
                        <ul class="list-unstyled stack-list">
                            <li>Xamarin</li>
                            <li>React Native</li>
                            <li>Ionic</li>
                            <li>PhoneGap</li>
                        </ul>
                    </div>
                    <div class="col-md-3">
                        <ul class="list-unstyled stack-list">
                            <li>Corona</li>
                            <li>Appcelerator</li>
                            <li>Sencha Ext JS</li>
                            <li>MonoCross</li>
                        </ul>
                    </div>
                    <div class="col-md-3">
                        <ul class="list-unstyled stack-list">
                            <li>Kony</li>
                            <li>Convertigo</li>
                            <li>NativeScript</li>
                            <li>RhoMobile Suite</li>
                        </ul>
                    </div>
                </div>
                <!-- <a href="#" class="btn mob-cod-btn mt-3">Know More</a> -->
            </div>
            <hr>
            <div id="design" class="sec-stack fifth-stack">
                <div class="stack-heading">
                    <img src="img/uiux.png"  alt="UI/UX Design">
                    <h3 class="android-dev mb-3">UI/UX Design</h3>
                </div>
                <p>Appsdelta believes in design-driven development which ensures we deliver the desirable product in
                    front of the user. We have a team of passionate designers and developers who create such engaging
                    design that tempts a user to return back to the website/app. Our acquaintance with the latest trend
                    enables us to predict a user’s mindset in advance.</p>
                <div class="row">
                    <div class="col-md-3">
                        <ul class="list-unstyled stack-list">
                            <li>Adobe Photoshop</li>
                            <li>Adobe Illustrator</li>
                            <li>Adobe Experience design</li>
                            <li>Sketch App</li>
                            <li>Adobe After Effects</li>
                        </ul>
                    </div>
                    <div class="col-md-3">
                        <ul class="list-unstyled stack-list">
                            <li>Figma</li>
                            <li>Flinto</li>
                            <li>Marvel</li>
                            <li>Invision</li>
                            <li>Balsamiq</li>
                        </ul>
                    </div>
                </div>
                <!-- <a href="product-design" class="btn mob-cod-btn mt-3">Know More</a> -->
            </div>
            <hr>
            <div id="node" class="sec-stack sixth-sec">
                <div class="stack-heading">
                    <img src="img/nodejs_development.png"  alt="Node.js Development">
                    <h3 class="android-dev mb-3">Node.js Development</h3>
                </div>
                <p>Node.js incorporates the power to build swifter and highly scalable real-time applications that can
                    strengthen your back-end. Our team at Appsdelta is highly skilled and has expertise in Node.js API
                    development which allows clients to be in sync with recent technological advancements. We use
                    Node.js to develop high performing web applications.</p>
                <div class="row">
                    <div class="col-md-4">
                        <ul class="list-unstyled stack-list">
                            <li>Node.js API Development and Integration</li>
                            <li>Node.js CMS Development</li>
                            <li>Node.js Server Side Development</li>
                            <li>Node.js Web App Development</li>
                        </ul>
                    </div>
                    <div class="col-md-4">
                        <ul class="list-unstyled stack-list">
                            <li>Node.js Plugin Development</li>
                            <li>Node.js Packages Development</li>
                            <li>Custom Node.js Application Development</li>
                        </ul>
                    </div>
                </div>
                <!-- <a href="nodejs" class="btn mob-cod-btn mt-3">Know More</a> -->
            </div>
            
        </div>
    </div>
			</div>
		</section>
		<!--/ End Services -->
		<script type="text/javascript">
		function scrollToDiv(id){
			document.getElementById(id).scrollIntoView({
			behavior: 'smooth'
			});
		}
		</script>
		<style>
		.stack-heading {
			position: relative;
		}
		.stack-heading img {
		width: 60px;
		height: 40px;
		position: absolute;
		top: -3px;
		left: -8px;
		}
		.stack-detail h3 {
			font-size: 30px;
			font-family: circe, sans-serif;
			color: #1b1d1f;
			left:55px;
		}
		</style>
		<!-- Footer -->
		<?php include('footer.php')?>